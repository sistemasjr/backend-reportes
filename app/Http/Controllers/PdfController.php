<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PdfController extends Controller
{
    public function VerPdf(Request $request)
    {
        $desde = date('Y-m-d H:i:s', $request->desdetimestamp);
        $hasta = date('Y-m-d H:i:s', $request->hastatimestamp);
        
        $datos = DB::table('new_embarque')->whereBetween('created_at', [$desde , $hasta])->get();
        $pdf = PDF::loadView('new_embarque',['datos' => $datos])->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
    
    public function DescargarPdf(Request $request)
    {
        $desde = date('Y-m-d H:i:s', $request->desdetimestamp);
        $hasta = date('Y-m-d H:i:s', $request->hastatimestamp);
        
        $datos = DB::table('new_embarque')
        ->select(
            'id',
            'fecha',
            'factura',
            'embarque',
            'cliente',
            'tipo',
            'tracto',
            'cajas',
            'ruta',)
        ->whereBetween('created_at', [$desde , $hasta])->get();
        return $datos;
    }

    // public function DescargarPdf()
    // {
    //     $datos = DB::table('reportes')->get();
    //     $pdf = PDF::loadView('reporte',['datos' => $datos])->setPaper('a4', 'landscape');
    //     return $pdf->stream();
    // }
}
