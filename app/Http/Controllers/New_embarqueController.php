<?php

namespace App\Http\Controllers;

use App\Models\new_embarque;
use Illuminate\Http\Request;

class New_embarqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $new_embarque = new_embarque::orderBy('id','desc')->get();
        return $new_embarque;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->validate(
            [
                'cliente'       => 'required',
                'factura'       => 'required',
                'embarque'      => 'required',
                'ruta'          => 'required',
                'tipo'          => 'required',
                'tracto'        => 'required',
                'chofer'        => 'required',
                'cajas'         => 'required',
                'efectivo'      => 'required',
                'observaciones' => 'required',
                'fecha'         => 'required',
            ],[
                'cliente.required'       =>'Campo es obligatorio',
                'factura.required'       =>'Campo es obligatorio',
                'embarque.required'      =>'Campo es obligatorio',
                'ruta.required'          =>'Campo es obligatorio',
                'tipo.required'          =>'Campo es obligatorio',
                'tracto.required'        =>'Campo es obligatorio',
                'chofer.required'        =>'Campo es obligatorio',
                'cajas.required'         =>'Campo es obligatorio',
                'efectivo.required'      =>'Campo es obligatorio',
                'observaciones.required' =>'Campo es obligatorio',
                'fecha.required'         =>'Campo es obligatorio',
            ]);

        $new_embarque = new new_embarque();
        $new_embarque->cliente = $request->cliente;
        $new_embarque->factura = $request->factura;
        $new_embarque->embarque = $request->embarque;
        $new_embarque->ruta = $request->ruta;
        $new_embarque->tipo = $request->tipo;
        $new_embarque->tracto = $request->tracto;
        $new_embarque->chofer = $request->chofer;
        $new_embarque->cajas = $request->cajas;
        $new_embarque->efectivo = $request->efectivo;
        $new_embarque->observaciones = $request->observaciones;
        $new_embarque->fecha = $request->fecha;
        $new_embarque->save();

        return response()
                ->json(['respuesta' => 'Datos guardados exitosamente.']);      
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\new_embarque  $new_embarque
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $resultado = new_embarque::find($id);
        return $resultado;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\new_embarque  $new_embarque
     * @return \Illuminate\Http\Response
     */
    public function edit(new_embarque $new_embarque)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\new_embarque  $new_embarque
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $new_embarque = new_embarque::findOrFail($id);
        $new_embarque->cliente = $request->cliente;
        $new_embarque->factura = $request->factura;
        $new_embarque->embarque = $request->embarque;
        $new_embarque->ruta = $request->ruta;
        $new_embarque->tipo = $request->tipo;
        $new_embarque->tracto = $request->tracto;
        $new_embarque->chofer = $request->chofer;
        $new_embarque->cajas = $request->cajas;
        $new_embarque->efectivo = $request->efectivo;
        $new_embarque->observaciones = $request->observaciones;
        $new_embarque->fecha = $request->fecha;
        
        $new_embarque->save();
        return response()
        ->json(['respuesta' => 'Datos actualizados exitosamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\new_embarque  $new_embarque
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $embarque = new_embarque::find($id);
        $embarque->delete();
        
    }
}
