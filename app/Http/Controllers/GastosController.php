<?php

namespace App\Http\Controllers;

use App\Models\Gasto;
use Illuminate\Http\Request;

class GastosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->validate(
            [
                'Tiket'         => 'required',
                'concepto'      => 'required',
                'cantidad'      => 'required',
                'precioUnitario' => 'required',
                'importe'       => 'required',
                'observaciones' => 'required',
            ],[
                'Tiket.required'         =>'Campo es obligatorio',
                'concepto.required'      =>'Campo es obligatorio',
                'cantidad.required'      =>'Campo es obligatorio',
                'precioUnitario.required' =>'Campo es obligatorio',
                'importe.required'       =>'Campo es obligatorio',
                'observaciones.required' =>'Campo es obligatorio',
            ]);

            $gasto = new Gasto();
            $gasto->Tiket         = $request->Tiket;
            $gasto->concepto      = $request->concepto;
            $gasto->cantidad      = $request->cantidad;
            $gasto->precioUnitario = $request->precioUnitario;
            $gasto->importe       = $request->importe;
            $gasto->observaciones = $request->observaciones;
            $gasto->save();

        return response()
                ->json(['respuesta' => 'Datos guardados exitosamente.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gasto  $gasto
     * @return \Illuminate\Http\Response
     */
    public function show(Gasto $gasto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gasto  $gasto
     * @return \Illuminate\Http\Response
     */
    public function edit(Gasto $gasto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gasto  $gasto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gasto $gasto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gasto  $gasto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gasto $gasto)
    {
        //
    }
}
