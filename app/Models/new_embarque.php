<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class new_embarque extends Model
{
    use HasFactory;

    protected $table= 'new_embarque';

    protected $fillable = [
        'cliente',
        'factura',
        'embarque',
        'ruta',
        'tipo',
        'tracto',
        'chofer',
        'cajas',
        'efectivo',
        'observaciones',
        'fecha',
    ];
}
