<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <title>Reporte</title>
    <style>
        .table{
            border: 1px solid;
            width: 100%;
        }
        .table tr{
          width: 100%;
        }
        .table th{
            text-align: center;
            border: 1px solid;
            font-size: 14px;
        }
        .table td{
            text-align: center;
            border: 1px solid;
            padding: 0px 30px 0px 30px;
            font-size: 12px;
        }
    </style>
</head>
<body>
    <h2 class="text-center mb-2">Reporte</h2>
    <table class="table" >
      <thead>
        <tr>
          <th>ID</th>
          <th>FECHA</th>
          <th>FACTURA</th>
          <th>EMBARQUE</th>
          <th>CLIENTE</th>
          <th>TIPO</th>
          <th>TRACTO</th>
          <th>CAJA</th>
          <th>LOGISTICA</th>
        </tr>
      </thead>
      <tbody>
       @foreach ($datos as $dato)
       <tr>
        <td>{{$dato->id}}</td>
        <td>{{$dato->fecha}}</td>
        <td>{{$dato->factura}}</td>
        <td>{{$dato->embarque}}</td>
        <td>{{$dato->cliente}}</td>
        <td>{{$dato->tipo}}</td>
        <td>{{$dato->tracto}}</td>
        <td>{{$dato->cajas}}</td>
        <td>{{$dato->ruta}}</td>
       </tr>
           
       @endforeach
      </tbody>
    </table>
    
    {{-- <table class="table table-striped">
        <thead>
          <tr>
            <th>Fecha</th>
            <th>Cliente</th>
            <th>Caja</th>
            <th>tipo</th>
            <th>Ruta</th>
            <th>km</th>
            <th>Pago de la ruta</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>30/12/2022 4:00PM</td>
            <td>YEREVAN</td>
            <td>ALFA 04</td>
            <td>SENCILLO</td>
            <td>NAVOJOA - NOG</td>
            <td>174</td>
            <td>$1,900.00</td>
          </tr>
  
          <tr>
            <td>29/12/2022 4:00PM</td>
            <td>VITCITRUS</td>
            <td>ALFA 01</td>
            <td>FULL</td>
            <td>COSTA - NOG</td>
            <td>250</td>
            <td>$2,300.00</td>
          </tr>
  
          <tr>
            <td>28/12/2022 4:00PM</td>
            <td>PROVINCIAS</td>
            <td>ALFA 03</td>
            <td>SENCILLO</td>
            <td>COSTA - NOG</td>
            <td>250</td>
            <td>$1,300.00</td>
          </tr>
  
          <tr>
            <td>27/12/2022 4:00PM</td>
            <td>NCO</td>
            <td>ALFA 09</td>
            <td>FULL</td>
            <td>NAVOJOA - NOG</td>
            <td>250</td>
            <td>$3,000.00</td>
          </tr>
          
        </tbody>
      </table> --}}
</body>
</html>