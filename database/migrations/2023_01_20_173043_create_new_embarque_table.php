<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewEmbarqueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_embarque', function (Blueprint $table) {
            $table->id();
            $table->string('cliente');
            $table->string('factura');
            $table->string('embarque');
            $table->string('ruta');
            $table->string('tipo');
            $table->string('tracto');
            $table->string('chofer');
            $table->string('cajas');
            $table->string('efectivo');
            $table->text('observaciones');
            $table->string('fecha');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_embarque');
    }
}
