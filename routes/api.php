<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\GastosController;
use App\Http\Controllers\New_embarqueController;
use App\Http\Controllers\PdfController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/* 
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(AuthController::class)->group(function () {
    Route::post('/register', 'register');
    Route::post('/login', 'login');
    Route::get('/token', 'token');

});

Route::controller(New_embarqueController::class)->group(function () {
    Route::get('/new_embarque','index'); 
    Route::post('/new_embarque','store');
    Route::get('/embarque/{id}','show');
    Route::put('/embarque/{id}','update');
    Route::delete('/embarque/{id}','destroy');
});

Route::controller(PdfController::class)->group(function () {
    Route::post('/verpdf','VerPdf');
    Route::post('/descargarpdf','DescargarPdf');

});

Route::controller(GastosController::class)->group(function () {
    Route::get('/tickets','index'); 
    Route::post('/nuevoticket','store');
    Route::get('/ticket/{id}','show');
    Route::put('/ticket/{id}','update');
    Route::delete('/tiket/{id}','destroy');

});

Route::middleware(['auth:sanctum'])->group(function () {
      
    Route::get('/logout',[AuthController::class, 'logout']);
});